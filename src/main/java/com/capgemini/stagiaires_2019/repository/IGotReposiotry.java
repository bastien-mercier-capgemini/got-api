package com.capgemini.stagiaires_2019.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.capgemini.stagiaires_2019.model.GotEntity;

@Repository
public interface IGotReposiotry extends CrudRepository<GotEntity, Integer> {

}
