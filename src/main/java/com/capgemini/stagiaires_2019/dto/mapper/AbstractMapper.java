package com.capgemini.stagiaires_2019.dto.mapper;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractMapper<T, V> {
	public List<V> mapAll(List<T> input) {
		if (input == null || input.size() == 0) {
			return Collections.emptyList();
		}
		return input.stream().map(x -> this.map(x)).collect(Collectors.toList());
	}

	public abstract V map(T input);
}
