package com.capgemini.stagiaires_2019.controller;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.capgemini.stagiaires_2019.exception.ApiError;
import com.capgemini.stagiaires_2019.exception.GenericException;

@RestControllerAdvice
public class ErrorController {

	@ExceptionHandler(GenericException.class)
	public ApiError handleGenericEx(GenericException ex) {
		ApiError error = new ApiError();
		error.setCode(ex.getArgs().get("code"));
		error.setMessage(ex.getArgs().get("message"));
		error.setDescription(ex.getArgs().get("description"));
		return error;
	}
}
