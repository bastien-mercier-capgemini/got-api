drop table if exists got;
create table if not exists got (
id integer auto_increment primary key, 
prenom varchar(50),
nom varchar(50), 
famille varchar(50), 
statut varchar(20));

insert into got (prenom, nom, famille, statut) values
('Jon', 'Snow', 'Targaryen', 'vivant'),
('Daenerys', 'Targaryen', 'Targaryen', 'mort'),
('Sansa', 'Stark', 'Stark', 'vivant'),
('Ramsay', 'Snow','Bolton', 'mort'),
('Arya', 'Stark', 'Stark', 'vivant'),
('Cersei', 'Lannister', 'Lannister', 'mort');
