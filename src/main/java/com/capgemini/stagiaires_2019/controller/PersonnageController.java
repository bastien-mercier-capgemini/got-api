package com.capgemini.stagiaires_2019.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.stagiaires_2019.business.IGotBusiness;
import com.capgemini.stagiaires_2019.dto.GotDto;
import com.capgemini.stagiaires_2019.dto.mapper.GotMapper;
import com.capgemini.stagiaires_2019.exception.GenericException;

@RestController
@RequestMapping("/got-api/personnage")
public class PersonnageController {
	@Autowired
	private IGotBusiness business;
	@Autowired
	private GotMapper mapper;

	@RequestMapping(method = RequestMethod.GET, value = "/liste")
	public List<GotDto> findAll() {
		return mapper.mapAll(business.findAll());
	}

	@RequestMapping(method = RequestMethod.GET, value = "/generic-exception")
	public Object exampleError() {
		throw new GenericException("E-F-0001", "exemple d'erreur", "detail de l'erreur");
	}
}
