package com.capgemini.stagiaires_2019.exception;

import java.util.HashMap;
import java.util.Map;

public class GenericException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Map<String, String> getArgs() {
		return args;
	}

	public void setArgs(Map<String, String> args) {
		this.args = args;
	}

	private Map<String, String> args;

	public GenericException() {
		super();
		ofDefault();
	}

	public GenericException(String... args) {
		super();
		ofArgs(args);
	}

	private void ofArgs(String... obj) {
		if (obj.length == 3) {
			args = new HashMap<>();
			args.put("code", obj[0]);
			args.put("message", obj[1]);
			args.put("description", obj[0]);
		} else {
			ofDefault();
		}
	}

	private void ofDefault() {
		args = new HashMap<>();
		args.put("code", "E-G-0001");
		args.put("message", "une exception inconnue est survenue");
	}
}
