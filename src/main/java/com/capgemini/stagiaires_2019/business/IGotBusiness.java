package com.capgemini.stagiaires_2019.business;

import java.util.List;

import com.capgemini.stagiaires_2019.model.GotEntity;

public interface IGotBusiness {
	List<GotEntity> findAll();
}
