package com.capgemini.stagiaires_2019.dto.mapper;

import org.springframework.stereotype.Component;

import com.capgemini.stagiaires_2019.dto.GotDto;
import com.capgemini.stagiaires_2019.model.GotEntity;

@Component
public class GotMapper extends AbstractMapper<GotEntity, GotDto> {

	@Override
	public GotDto map(GotEntity input) {
		if (input == null) {
			return null;
		}
		GotDto dto = new GotDto();
		dto.setId(input.getId());
		dto.setFamille(input.getFamille());
		dto.setNom(input.getNom());
		dto.setPrenom(input.getPrenom());
		dto.setStatut(input.getStatut());
		return dto;
	}

}
