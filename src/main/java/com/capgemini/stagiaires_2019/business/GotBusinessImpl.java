package com.capgemini.stagiaires_2019.business;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.stagiaires_2019.model.GotEntity;
import com.capgemini.stagiaires_2019.repository.IGotReposiotry;

@Service
public class GotBusinessImpl implements IGotBusiness {
	@Autowired
	private IGotReposiotry repo;

	@Override
	public List<GotEntity> findAll() {
		// TODO Auto-generated method stub
		return StreamSupport.stream(repo.findAll().spliterator(), false).collect(Collectors.toList());
	}

}
